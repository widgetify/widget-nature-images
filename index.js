//from https://rominirani.com/using-puppeteer-in-google-cloud-functions-809a14856e14
//Deploy with command:$ gcloud functions deploy --region=europe-west1 --runtime=nodejs8 --memory=1024MB --trigger-http pipeAnimals
//Check its https trigger with $ gcloud functions describe pipeAnimals

/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */

exports.pipeAnimals = (req, res) => {

const puppeteer = require('puppeteer');
var https       = require('https');
const cheerio   = require('cheerio')

const {Storage} = require('@google-cloud/storage');
// var fs = require('fs');
// var request = require('request');


const bucketName  = 'bucket-for-photos';
const bucketName2 = 'bucket-for-current-photos';

const storage = new Storage({
    projectId: 'zeta-sky-267811',
    keyFilename: 'zeta-sky-267811-5bd19e5ab502.json',
});

// function download(url, filename, callback) {
//     request.head(url, function (err, res, body) {
//         request(url).pipe(fs.createWriteStream(filename)).on('close', callback);
//     });
// };

// async function uploadFile(fn) {
//     await storage.bucket(bucketName).upload(fn, {
//         gzip: true,
//         metadata: {
//             cacheControl: 'public, max-age=31536000',
//         },
//     });
//     console.log(`${fn} uploaded to ${bucketName}.`);
// }


function getImageURL() {
    return new Promise(async (resolve, reject) => {
        try {
            const browser = await puppeteer.launch({
                args: ['--no-sandbox']
            });
            const page = await browser.newPage();
            await page.goto('https://www.reddit.com/r/NatureIsFuckingLit/');
            var postArray = await page.evaluate(el => {
                let posts = Array.from(document.querySelectorAll('.Post'));
                let postsArray = posts.map(element => {
                    return element.innerHTML
                })
                return postsArray;
            })
            browser.close();

            console.log('postArray:', postArray.length)
            for (i = 0; i < postArray.length; i++) {
                if (!postArray[i].includes('ImageBox-image') || !postArray[i].includes('media-element') || postArray[i].includes('iframe')) {
                    delete postArray[i]
                }
            }
            var postArray2 = postArray.filter(item => item != undefined)
            console.log('postArray:', postArray2.length)
            console.log('postArray2[0]:', postArray2[0])
            var titles = []
            var urls = []
            for (i = 0; i < postArray2.length; i++) {
                var $ = cheerio.load(postArray2[i])
                var title = $('h3').text()
                title = title.replace(/ /g, '_')
                title = title.replace(/\W/g, '')
                var url = $('.ImageBox-image.media-element').attr('src')
                // var urlStart = url.indexOf('.it/') + 4
                // var urlName = 'https://i.redd.it/' + url.substr(urlStart, 17)
                titles.push(title)
                urls.push(url)
            }
            
            console.log('titles:', titles)
            console.log('urls:', urls)
            
            return resolve([titles, urls]);
        } catch (e) {
            return reject(e);
        }
    })
}

getImageURL()
    .then(postInfo => {
        var titles = postInfo[0]
        var urls = postInfo[1]
        const bucket = storage.bucket(bucketName);
        const bucket2 = storage.bucket(bucketName2);
        for (i = 0; i < titles.length; i++) {
            var imageName = titles[i] + '.jpg'
            const file = bucket.file(imageName);
            const file2 = bucket2.file(imageName);
            https.get(urls[i], function (res) {
                for (j=0;j<10;j++){
                    if (res.headers['content-length']>20000){
                        res.pipe(
                            file.createWriteStream({
                                resumable: false,
                                public: true,
                                metadata: {
                                    contentType: res.headers["content-type"]
                                }
                            })
                        );
                        break
                    }
                }
            });
            https.get(urls[i], function (res) {
                for (k=0;k<10;k++){
                    if (res.headers['content-length']>20000){
                        res.pipe(
                            file2.createWriteStream({
                                resumable: false,
                                public: true,
                                metadata: {
                                    contentType: res.headers["content-type"]
                                }
                            })
                        );
                        break
                    }
                }
            });
        }
    })
    .catch(err => {
        console.error("un error", err);
    })

  };